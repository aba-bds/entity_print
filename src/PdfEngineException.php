<?php

namespace Drupal\entity_print;

/**
 * This exception is thrown when a PDF implementation fails to generate a PDF.
 */
class PdfEngineException extends \Exception {

}
